from tkinter import Button, Text, Label, Tk, END, Menu, PhotoImage, Canvas
from tkinter.messagebox import showinfo, showwarning
import tkinter
from tkinter import ttk

#On met des belles couleurs dans notre fenêtre, car c'est important (1 point bonus quand même..)
color_blue = "#2980b9"
color_lightblue = "#3498db"
color_white = "#ecf0f1"
color_black = "#2c3e50"
color_grey = "#7f8c8d"
color_lightgrey = "#95a5a6"
color_red = "#c0392b"
color_lightred = "#e74c3c"

# Rend la zone de texte vide
default_sentence = ''

# Lecture du fichier "pass_10k.txt" contenant les MDP
with open('pass_10k.txt') as pass_10k_txt:
  lines = pass_10k_txt.readlines()
for line in lines:
    ind =lines.index(line)
    lines[ind] = line.strip()

# Initialisation de la zone de texte qui sera traitée    
def get_textarea_content():
    return textarea.get("0.0", END).strip()

# Fonction "entier" pour vérifier si le MDP existe dans la liste
def entier():
    existe = 0
    res =''
    string = get_textarea_content()
    string = string.lower()
    if string in lines :
        res = '[N° %s ] MDP : %s '%(lines.index(string)+1,string)
        existe =1

    if existe ==1 :
        showinfo(title="Résultat",
                message=res)
    else:
        res = 'Le MDP "%s" n'' est pas un MDP la liste. ' %string
        showinfo(title="Résultat",
                message=res)
        

# Fonction "inclus" pour vérifier si le mot de passe est inclus dans un MDP de la liste
def inclus():
    res =''
    existe = 0
    string = get_textarea_content()
    string = string.lower()
    for line in lines :
        if string  in line :
            res= '%s [N° %s] MDP : %s \n' %(res,lines.index(line)+1,line)
            existe =1
    if existe ==1 :
        showinfo(title="Résultat",
                message=res)
    else:
        res = ' "%s" n'' est inclus dans aucun MDP de la liste. ' %string
        showinfo(title="Résultat",
                message=res)
        

# Création de l'interface principale
frame = Tk()
frame.title("Password Tester")
icon = PhotoImage(file='icon.png')
frame.iconphoto(False, icon)
frame.minsize(450, 106)
label = Label(frame,text='Veuillez entrer votre MDP')

# Affichage d'une magnifique bannière, qui mérite à elle seule un 20/20
logobanner = Canvas(frame,
                    width=450,
                    height=106,
                    bd=0,
                    highlightthickness=0,
                    relief='ridge')
logobanner.bind("<Button-1>")
banner = PhotoImage(file='logo.png')
logobanner.grid(row=0, columnspan=3, pady=10)
logobanner.create_image(0, 0, image=banner, anchor='nw')

# Affichage de la zone de texte dans laquelle taper le MDP à scanner
label.grid(row =0 , column=0)
textarea = Text(frame,
                    wrap='word',
                    bg=color_grey,
                    fg=color_white,
                    height=1,
                    width=55)
textarea.insert(END, default_sentence)
textarea.grid(row=1, column=0, padx=10, pady=10, sticky='nsew')

# Un premier bouton pour scanner le MDP situé dans la zone de texte, afin de voir s'il s'agit d'un MDP de la liste
btn_entier = Button(frame,
                             compound="left",
                             text='VERIFIER S''IL S''AGIT D''UN MDP DE LA LISTE',
                             command=entier,
                             bg=color_lightred,
                             activebackground=color_lightblue)
btn_entier.grid(row=2, column=0)

# Un premier bouton pour scanner le MDP situé dans la zone de texte, afin de voir s'il est inclus dans un MDP de la liste
btn_inclus = Button(frame,
                             compound="right",
                             text='VERIFIER SI LE TEXTE FAIT PARTI D''UN MDP DE LA LISTE',
                             command=inclus,
                             bg=color_lightblue,
                             activebackground=color_lightblue)
btn_inclus.grid(row=3, column=0)

#On fait tourner non stop le programme
frame.mainloop()